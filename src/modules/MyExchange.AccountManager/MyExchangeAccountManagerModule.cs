﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Kis.Dss
{
    [DependsOn(typeof(MyExchangeApplicationModule))]
    public class MyExchangeAccountManagerModule: AbpModule
    {

        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyExchangeAccountManagerModule).GetAssembly());
        }

        public override void PostInitialize()
        {

        }
    }
}
