﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Abp.Application.Services;
namespace Kis.Dss.Services
{
    /// <summary>
    /// Источник  https://github.com/AlexMAS/GostCryptography
    /// </summary>
    class GostSignatureApplicationService : KisAppServiceBase //, ISignatureApplicationService TODO временно отключено, пока не будет произведена настройка
    {
        public Task<string> SingXml(string document, SignerInfo signerInfo)
        {
            
            X509Certificate2 certificate = FindGostCertificate();

            if (document == null) throw new ArgumentException(nameof(document));

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(document);

            // Создание подписчика XML-документа
            var signedXml = new GostSignedXml(xmlDocument);

            // Установка ключа для создания подписи
            signedXml.SetSigningCertificate(certificate);

            // Ссылка на узел, который нужно подписать, с указанием алгоритма хэширования
            var dataReference = new Reference { Uri = "", DigestMethod = GetDigestMethod(certificate) };

            // Установка ссылки на узел
            signedXml.AddReference(dataReference);

            // Установка информации о сертификате, который использовался для создания подписи
            var keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(certificate));
            signedXml.KeyInfo = keyInfo;

            // Вычисление подписи
            signedXml.ComputeSignature();

            // Получение XML-представления подписи
            var signatureXml = signedXml.GetXml();

            // Добавление подписи в исходный документ
            xmlDocument.DocumentElement.AppendChild(xmlDocument.ImportNode(signatureXml, true));

            document = xmlDocument.OuterXml;

            return Task.FromResult(document);

        }

        private static string GetDigestMethod(X509Certificate2 certificate)
        {
            // Имя алгоритма вычисляем динамически, чтобы сделать код теста универсальным

            using (var publicKey = (GostAsymmetricAlgorithm)certificate.GetPublicKeyAlgorithm())
            using (var hashAlgorithm = publicKey.CreateHashAlgorithm())
            {
                return hashAlgorithm.AlgorithmName;
            }
        }

        /// <summary>
        /// Извлечение сертификата из хранилища
        /// </summary>
        /// <param name="storeName"></param>
        /// <param name="storeLocation"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        [SecuritySafeCritical]
        private static X509Certificate2 FindGostCertificate(StoreName storeName = StoreName.My, StoreLocation storeLocation = StoreLocation.LocalMachine, Predicate<X509Certificate2> filter = null)
        {
            var store = new X509Store(storeName, storeLocation);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            try
            {
                foreach (var certificate in store.Certificates)
                {
                    if (certificate.HasPrivateKey && certificate.IsGost() && (filter == null || filter(certificate)))
                    {
                        return certificate;
                    }
                }
            }
            finally
            {
                store.Close();
            }

            return null;
        }
    }
}
