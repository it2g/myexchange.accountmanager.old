using Kis.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Kis.Dss.WebHost.Controllers
{
    public class HomeController : KisControllerBase
    {

        public HomeController()
        {
        }

        public IActionResult Index() => Redirect("/swagger");

       
    }
}
