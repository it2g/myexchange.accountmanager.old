using Kis.Controllers;
using Microsoft.AspNetCore.Antiforgery;

namespace Kis.Dss.WebHost.Controllers
{
    public class AntiForgeryController : KisControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
