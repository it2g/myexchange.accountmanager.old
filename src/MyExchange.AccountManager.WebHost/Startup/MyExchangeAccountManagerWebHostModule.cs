﻿using Abp.Hangfire;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Kis.Dss.WebHost.Startup
{
    [DependsOn(typeof(AbpHangfireAspNetCoreModule),
               typeof(MyExchangeAccountManagerModule),
               typeof(MyExchangeWebCoreModule))]
    public class MyExchangeAccountManagerWebHostModule: AbpModule
    {
        public override void PreInitialize()
        {
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(MyExchangeAccountManagerWebHostModule).GetAssembly());
        }

        public override void PostInitialize()
        {
        }
    }
}
