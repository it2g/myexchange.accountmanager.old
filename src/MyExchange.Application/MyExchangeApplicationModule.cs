﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Kis.Authorization;

namespace Kis
{
    [DependsOn(
        typeof(MyExchangeCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class MyExchangeApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<KisAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(MyExchangeApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
